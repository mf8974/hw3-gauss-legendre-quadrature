# Numerical mathematics hw3: Gauss-Legendre quadratures

In this homework we implemented Gauss-Legendre quadrature algorithm for finding area under given function (curve).

## How to build the program
Go into the /code directory and use your favorite C/C++ compiler as shown bellow.

* GCC
```bash
# build example
gcc example.cpp -o example
# produces example.exe on Win and example on Unix

# build tests
gcc tests.cpp -o tests
```

* clang
```bash
# build example
clang example.cpp -o example
# produces example.exe on Win and example on Unix

# build tests
clang tests.cpp -o tests
```
