// NOTE(miha): Include our library for calculating integrals.
#include "integral.h"

// NOTE(miha): This is how we create function of which we want to calculate
// integral (area).
f64
Square(f64 X)
{
    return X*X;
}

// NOTE(miha): This is an example of creating more complicated function (taken
// from homework 3).
f64
HWFunction(f64 X)
{
    f64 Result = .0f;
    Result = sin(X)/X;
    return Result;
}

// NOTE(miha): This is the entry point of the example program.
int
main()
{
    // NOTE(miha): In the code bellow we calculate area of the square function
    // on the interval [0, 2]. Two Gauss-Legendre points are used for
    // approximation. This means that the square function is exactly
    // approximated with this function, because Gauss-Legrende on two points
    // can exactly approximate polynomials of degree 3 (2n - 1).
    f64 TwoPointApproximation = GaussLegendreTwoPoints(&Square, 0.0f, 2.0f);
    printf("A) Exact area of square function on interval [0, 2] is %f\n", TwoPointApproximation);

    // NOTE(miha): We can also use this function to approximate integral on a
    // given number of Gauss-Legrende points. In this case the number of points
    // is 2, result will be the same as above. Note that result would be
    // differen iff we would use only 1 point.
    f64 TwoPointApproximationAnotherWay = GaussLegendre(&Square, 0.0f, 2.0f, 2);
    printf("B) Exact area of square function on interval [0, 2] is %f\n", TwoPointApproximation);

    // NOTE(miha): We can use the following function to determine the min
    // number of points we need to approximate integral exactly on 10 digits.
    // First two number defines our search space for bisection.
    u32 NumberOfPointsForTenDigitApproximation = CalculateExactFor10Digits(1, 10, &Square, 0.0f, 2.0f);
    printf("For approximating square function on interval [0, 2] on 10 digits we need %d points\n", NumberOfPointsForTenDigitApproximation);

    // NOTE(miha): Now lets try to find the minimal number of points needed for
    // approximating the function for the homework (HWFunction).
    u32 Steps = CalculateExactFor10Digits(1, 100, &HWFunction, 0.0f, 5.0f);
    printf("Homework function can be correcltly approximated on 10 digits after using %d points\n", Steps);

    return 0;
}
