#include <stdio.h>
#include "integral.h"

// NOTE(miha): This unit testing library is copied from:
// https://jera.com/techinfo/jtns/jtn002.
#define mu_assert(message, test) do { if (!(test)) return message; } while (0)
#define mu_run_test(test) do { char *message = test(); tests_run++; \
                               if (message) return message; } while (0)
extern int tests_run;
int tests_run = 0;

bool 
ApproxEquals(f64 Value, f64 Other, f64 Epsilon)
{
    return fabs(Value - Other) < Epsilon;
}

// NOTE(miha): Some test function we have found on youtube (TODO: paste link).
f64
TestFunction(f64 X)
{
    f64 Result = .0f;
    Result = 5.0 * X * (pow(E, -(2.0*X)));
    return Result;
}

f64
HWFunction(f64 X)
{
    f64 Result = .0f;
    Result = sin(X)/X;
    return Result;
}

static char *
TestTwoPointsGaussLegendre()
{
    f64 Result = 0.0f;

    Result = GaussLegendreTwoPoints(&TestFunction, 0.1f, 1.3f);
    mu_assert("Integral TestFunction [0.1, 1.3] == 0.910183", ApproxEquals(Result, 0.910183, 0.00001));

    mu_assert("Integral TestFunction [0.1, 1.3] evaluated on TwoPoint-GL and NPoint-GL", 
              ApproxEquals(Result, GaussLegendre(&TestFunction, 0.1, 1.3, 2), 0.00001));

    return 0;
}

static char *
TestMultiplePoints()
{
    f64 Result = 0.0f;

    Result = GaussLegendre(&HWFunction, 0.0f, 5.0f, 1);
    mu_assert("Integral HWFunction [0.0, 5.0] == 1.196944", ApproxEquals(Result, 1.196944, 0.00001));

    Result = GaussLegendre(&HWFunction, 0.0f, 5.0f, 2);
    mu_assert("Integral HWFunction [0.0, 5.0] == 1.604526", ApproxEquals(Result, 1.604526, 0.00001));

    Result = GaussLegendre(&HWFunction, 0.0f, 5.0f, 3);
    mu_assert("Integral HWFunction [0.0, 5.0] == 1.547296", ApproxEquals(Result, 1.547296, 0.00001));

    Result = GaussLegendre(&HWFunction, 0.0f, 5.0f, 8);
    mu_assert("Integral HWFunction [0.0, 5.0] == 1.549931", ApproxEquals(Result, 1.549931, 0.00001));

    return 0;
}

static char *
TestExactApproximation()
{
    u32 Result = 0;

    Result = CalculateExactFor10Digits(1, 50, &HWFunction, 0.0, 5.0);
    mu_assert("Integral HWFunction [0.0, 5.0] need 8 steps for 10 digit approx", Result == 8);

    return 0;
}

static char *
RunTests()
{
    mu_run_test(TestTwoPointsGaussLegendre);
    mu_run_test(TestMultiplePoints);
    mu_run_test(TestExactApproximation);

    return 0;
}

int
RunTestsAndStatistics()
{
    char *Result = RunTests();

    if(Result != 0)
    {
        printf("%s\n", Result);
    }
    else
    {
        printf("ALL TESTS PASSED\n");
    }
    
    printf("Tests run: %d\n", tests_run);

    return Result != 0;
}

int
main()
{

    RunTestsAndStatistics();

    return 0;
}
