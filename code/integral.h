#ifndef INTEGRAL_H
#define INTEGRAL_H

#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <float.h>

#if DEBUG
#define Assert(Expression) if(!(Expression)) {*(int *)0 = 0;}
#else
#define Assert(Expression)
#endif

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef float f32;
typedef double f64;

typedef f64 (*function)(f64);

#define E 2.7182818284590452353602874f
#define PI 3.14159265358979323846f

// NOTE(miha): This is a formula for Gauss-Legendere quadrature on two points.
// We are integrating function 'Function' on interval ['A', 'B'].
f64
GaussLegendreTwoPoints(function Function, f64 A, f64 B)
{
    f64 C1 = (B-A)/2.f;
    f64 C2 = (B-A)/2.f;

    f64 X1 = ((B-A)/2.f) * (-(1.f/sqrt(3))) + ((B+A)/2);
    f64 X2 = ((B-A)/2.f) * (1.f/sqrt(3)) + ((B+A)/2);

    f64 F1 = Function(X1);
    f64 F2 = Function(X2);

    f64 Result = C1*F1 + C2*F2;

    return Result;
}

// NOTE(miha): This function calculates weights and abscissas (roots) for
// Gauss-Legendre quadrature on 'N' points. Interval is given from 'A' to 'B'.
// Result is returned via reference in values Abscissas and Weights (these two
// arrays need to have length 'N').
void
CalculateAbscissasAndWeights(u32 N, f64 A, f64 B, f64 *Abscisssas, f64 *Weights)
{
    f64 Epsilon = 1.0e-15f;

    // NOTE(miha): 'M' is a number of roots we need to find. Because they are
    // symetric we only need to find half of them!
    u32 M = (N+1)/2;
    f64 XM = 0.5f * (B+A);
    f64 XL = 0.5f * (B-A);

    f64 P1, P2, P3, dRoot, Z1;

    for(u32 Index = 0; Index < M; ++Index)
    {
        // NOTE(miha): Approximation for 'Indext'h root.
        f64 Root = cos(3.141592654f * (Index+0.75f)/(N+0.5f));

        do
        {
            P1 = 1.0f;
            P2 = 0.0f;

            // NOTE(miha): Calculate Legendre polynomail at 'Root'.
            for(u32 J = 0; J < N; ++J)
            {
                P3 = P2;
                P2 = P1;
                P1 = ((2.0f*J + 1.0) * Root*P2 - J*P3)/(J+1);
            }

            // NOTE(miha): 'dRoot' is a derivative of 'Root'.
            dRoot = N * (Root*P1 - P2)/(Root*Root - 1.0);
            Z1 = Root;

            // NOTE(miha): Newtons method 
            Root = Z1 - (P1/dRoot);
        }
        while(fabs(Root-Z1) > Epsilon);
        
        // NOTE(miha): We need to scale back the root to the desired interval
        // and calculate weight. Also put values into the symetric counterpart.
        Abscisssas[Index] = XM - XL*Root;
        Abscisssas[N-1-Index] = XM + XL*Root;
        Weights[Index] = 2.0*XL/((1.0-Root*Root)*dRoot*dRoot);
        Weights[N-1-Index] = Weights[Index];
    }
}

// NOTE(miha): Here we calculate Gauss-Legendre integral approximation of
// function 'Function' on 'N' points. Interval is from 'A' to 'B'. Returned
// result is the area under the curve on the given interval.
f64
GaussLegendre(function Function, f64 A, f64 B, u32 N)
{
    f64 *Abscissas = (f64 *) malloc(sizeof(f64) * (N));
    f64 *Weights = (f64 *) malloc(sizeof(f64) * (N));
    CalculateAbscissasAndWeights(N, A, B, Abscissas, Weights);

    f64 XM = 0.5f * (B+A);
    f64 XR = 0.5f * (B-A);
    f64 S = .0f;

    for(u32 I = 0; I < N; ++I)
    {
        S += Weights[I] * Function(Abscissas[I]);
    }

    free(Abscissas);
    free(Weights);

    return S;
}

// NOTE(miha): This function calculates how many steps we need to approximate
// integrals value exact on 10 digits. It uses bisection to find the result. On
// function 'Function' we want to calculate integral from 'A' to 'B'. 'Min' and
// 'Max' set the search space for the bisection.
u32
CalculateExactFor10Digits(u32 Min, u32 Max, function Function, f64 A, f64 B)
{
    f64 ExactResult = GaussLegendre(Function, A, B, 1000);
    f64 Result = 0.0f;

    u32 Middle = (Min+Max)/2;
    u32 MinSteps = Middle;

    while(Min != (Max-1))
    {
        Result = GaussLegendre(Function, A, B, Middle);

        if(fabs(ExactResult - Result) < .1e-10)
        {
            Max = Middle;
        }
        else
        {
            Min = Middle;
        }

        Middle = (Min+Max)/2;
        MinSteps = Middle;
    }

    // printf("result: %f\n", Result);

    return MinSteps+1;
}

#endif // INTEGRAL_H
