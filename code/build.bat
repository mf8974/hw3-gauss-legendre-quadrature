@echo off

set CommonCompilerFlags=-MTd -nologo -Gm- -GR- -EHa- -Od -Oi -WX -W4 -wd4201 -wd4100 -wd4189 -wd4505 -DDEBUG=1 -FC -Z7
set CommonLinkerFlags= /NODEFAULTLIB:library -incremental:no -opt:ref Winmm.lib gdi32.lib kernel32.lib user32.lib shell32.lib

IF NOT EXIST ..\build mkdir ..\build
pushd ..\build

cl %CommonCompilerFlags% ..\code\integral.cpp Winmm.lib gdi32.lib opengl32.lib kernel32.lib user32.lib shell32.lib -Fmplatform.map /link %CommonLinkerFlags%

REM cl -FC -Zi main.c -I"..\..\includes

popd
